Security
========
This playbook hardens a system, mostly attempting to set recommendations found in Enterprise Linux 7/8 in both the NIST and CIS guides. It is **HIGHLY** encouraged to look over those documents. Even a quick skim looking at some of the parameters set could be interesting and improve understanding of this playbook.
https://static.open-scap.org/ssg-guides/ssg-centos8-guide-index.html
https://static.open-scap.org/ssg-guides/ssg-rhel7-guide-C2S.html
https://secscan.acron.pl/centos7/start

The NIST guides are open with no registration or payment. Due to that fact, it's highly recommended to use those guides instead of the "CIS benchmark". The documentation around the NIST Guides provides shell and ansible commands to check and correction mechanisms.

The "futures" tag is used to designate an improvement, and that it could disrupt normal operations. For example adding the fapolicy package, could make the machine slower or prevent access (due to an overly strict policy). 

### Passwords.yml
This file contains all controls relating to passwords. Much of this is purely due to the fact that the guides recommend/mandate these controls and it could be graded. By not even having passwords set, we avoid a lot of pain around this. Use certificates, and two factor. Often users don't even remember their passwords instead they write them in a password store, thus breaking the idea of "something the user has" (device, like 2fa) and "something the user knows" (password).

### auditd.yml
many controls relating to auditd

Role Variables
--------------
A user would be much advised to look over the defaults (in defaults/main.yml). Many of these are pulled from the NIST and CIS guides; they can be overwritten, thus ensuring modularity.


Requirements
------------
See Dependencies

Dependencies
------------
The "common" playbook should be ran. The machine is presumed to have certificates trusted, packages updated, repositories modified, rsyslog and ssh configured. It is _also_ presumed that this is coming from a minimal installation. One should not rerun this playbook on production systems, as it could be slow. This should be ran once at build time. However it should be written that rerunning this would not cause any ill affect other than slowness. 

Example Playbook
----------------

    - hosts: localhost
      roles:
         - { role: linux_security, auditd_commands: ['rpm'] }

License
-------

BSD

Author Information
------------------
Kevin Faulkner

